import models from "../models";



export default {
  //TODO Agregar un Candidatos
  add: async (req, res, next) => {
    try {
      
      const reg = await models.Candidatos.create(req.body);
      res.status(200).json(reg);
    } catch (error) {
      res.status(500).send({
        message: "Ocurrio un error",
      });
      next(error);
    }
  },
  //TODO colsultar el Candidatos
  query: async (req, res, next) => {
    try {
      const reg = await models.Candidatos.findOne({
        _id: req.query._id,
      });
      if (!reg) {
        res.status(404).send({
          message: "El registro no existe",
        });
      } else {
        res.status(200).json(reg);
      }
    } catch (error) {
      res.status(500).send({
        message: "Ocurrio un error",
      });
      next(error);
    }
  },
  //TODO listar Candidatoss
  list: async (req, res, next) => {
    try {
      let valor = req.query.valor;
      const reg = await models.Candidatos.find(
        {
          $or: [
            { name: new RegExp(valor, "i") },
            { age: new RegExp(valor, "i") },
          ],
        },
        
      )
        
        
      res.status(200).json(reg);
    } catch (error) {
      res.status(500).send({
        message: "Ocurrio un error",
      });
      next(error);
    }
  },
  //TODO actualizar Candidatos
  update: async (req, res, next) => {
    try {
      

      const reg = await models.Candidatos.findByIdAndUpdate(
        { _id:req.body._id },
        {
          name: req.body.name,
          fecha_entrevista: req.body.fecha_entrevista,
          habilidades: req.body.habilidades,
          
        }
      );
      res.status(200).json(reg);
    } catch (error) {
      res.status(500).send({
        message: "Ocurrio un error",
      });
      next(error);
    }
  },
  //TODO eliminar Candidatos
  remove: async (req, res, next) => {
    try {
        const reg = await models.Candidatos.findOneAndDelete({_id:req.body._id});
      res.status(200).json(reg);
    } catch (error) {
      res.status(500).send({
        message: "Ocurrio un error",
      });
      next(error);
    }
  },
 

  

 
};
