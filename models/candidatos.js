import mongoose,{Schema} from 'mongoose';


const CandidatosSchema = new Schema({

    name: {
        type:String,
        maxlength: 250,
    },


    fecha_entrevista: {
        type:String,
        required:true,
        maxlength: 250,
    },
    habilidades: [
    
    ], 
    

});

const Candidatos = mongoose.model('candidatos', CandidatosSchema);
export default Candidatos;