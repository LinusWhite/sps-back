import routerx from 'express-promise-router';
import Candidatos from '../controllers/candidatos';


const router=routerx();
//SuperAdmin
router.post('/add',Candidatos.add);
router.get('/query',Candidatos.query);
router.get('/list', Candidatos.list);
router.put('/update', Candidatos.update);
router.delete('/remove', Candidatos.remove);



export default router;