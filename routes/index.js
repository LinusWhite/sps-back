import routerx from 'express-promise-router';

import candidatosRouter from './candidatos';


const router=routerx();


router.use('/candidatos',candidatosRouter);

export default router;